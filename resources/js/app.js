/**
 * Define some variables to use throughout the script
 */
const rootPath = process.env.MIX_APP_PATH;

/**
 * FontAwesome setup
 */
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faPlus, faSync } from '@fortawesome/free-solid-svg-icons';

library.add(faPlus, faSync);
dom.watch();

/** Setup WebFont Loader */
var WebFont = require('webfontloader');
WebFont.load({
    google: {
        families: ['Raleway:300,400,700', 'Roboto Slab:300,400,700']
    }
})

/**
 * Prep loadCSS for non-critical CSS loading
 * NoScript tag in header in case this fails
 */
import { loadCSS } from 'fg-loadcss';
loadCSS(rootPath+'css/app.css');

/**
 * Require vue.js
 */
window.Vue = require('vue');

/**
 * Make Axios available to all components
 */
const axios = require('axios');
Vue.prototype.$http = axios;

/**
 * Make my root path available to all
 * 
 */
Vue.prototype.$rootPath = rootPath;

/**
 * Begin vue.js component imports
 */
import DashboardTotalFilms from './components/Dashboard-TotalFilms.vue';
import DashboardQuickAdd from './components/Dashboard-QuickAdd.vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

/**
 * Gloabl vue.js components
 */
Vue.component('font-awesome-icon', FontAwesomeIcon);

/**
 * Now we register a brand new vue.js instance to the #ml-app container
 */
const app = new Vue({
    el: '#ml-app',
    data: {
        quicklinks: [
            { text: 'Add', link: rootPath+'app/add', subMenu: [
                { text: '<font-awesome-icon :icon="plus"></font-awesome-icon> Add from Form', link: rootPath+'app/add/form'},
                { text: '<font-awesome-icon :icon="plus"></font-awesome-icon> Add from Barcode', link: rootPath+'app/add/barcode'}
            ]},
            { text: 'List All', link: rootPath +'app/list' },
            { text: 'Settings', link: rootPath +'app/settings' }
        ],
        totalfilms: ''
    },
    components: { 
        'dashboard-total-films': DashboardTotalFilms,
        'dashboard-quick-add': DashboardQuickAdd
    }
});
