<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>medialib</title>

        <style>
            <?php include('css/app-critical.css'); ?>
        </style>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/modern-normalize/0.5.0/modern-normalize.min.css" />


        <noscript>
            <link rel="stylesheet" href="{{asset('css/app.css')}}" />
        </noscript>

    </head>
    <body>

        <div id="ml-app">

            <header class="ml-header">

                <a href="{{route('app')}}" class="logo">medialib</a>

                <ul class="ml-nav">
                    <li v-for="item in quicklinks" class="ml-nav__item">
                        <a :href="item.link" class="ml-nav__link">@{{ item.text }}</a>

                        <ul v-if="item.subMenu" class="ml-subnav">
                            <li v-for="subItem in item.subMenu" class="ml-subnav__item">
                                <a :href="subItem.link" class="ml-subnav__link">@{{subItem.text}}</a>
                            </li>
                        </ul>
                    </li>
                    <a href="{{route('logout')}}" class="ml-nav__link ml-nav__link--logout">Logout</a>
                </ul>

            </header>

            <section class="ml-panel-container">
                <dashboard-total-films></dashboard-total-films>
                <dashboard-quick-add></dashboard-quick-add>
            </section>



        </div>

        
        <script src="{{asset('js/manifest.js')}}"></script>
        <script src="{{asset('js/vendor.js')}}"></script>
        <script src="{{asset('js/app.js')}}"></script>
        <script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.24.7'><\/script>".replace("HOST", location.hostname));
//]]></script>
    </body>
</html>
