<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>medialib</title>

        <style>
            <?php include('css/app-critical.css'); ?>
        </style>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/modern-normalize/0.5.0/modern-normalize.min.css" />


        <noscript>
            <link rel="stylesheet" href="{{asset('css/app.css')}}" />
        </noscript>
    </head>

    <body class="login-screen">

        <h1 class="login-screen__title">medialib</h1>
        <div id="app">
        </div>

        <form class="login-form" method="POST" action="{{route('login')}}">
            @csrf

            <label for="email" class="login-form__label">
                {{ __('E-Mail Address') }}
                <input type="text" name="email" class="login-form__input {{ $errors->has('email') ? 'login-form__input--error' : '' }}" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </label>

            <label for="password" class="login-form__label">
                {{ __('Password') }}
                <input type="password" name="password" class="login-form__input {{ $errors->has('password') ? 'login-form__input--error' : '' }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </label>   

            <label class="login-form__label" for="remember">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                {{ __('Remember Me') }}
            </label>

            <button type="submit" class="btn btn--white">
                {{ __('Login') }}
            </button>
        </form>
        
        <script src="{{asset('js/manifest.js')}}"></script>
        <script src="{{asset('js/vendor.js')}}"></script>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
