<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\DB;

// Login Routes
Route::get('/', ['uses' => 'Auth\LoginController@LoginForm'])->name('applogin');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

// Some API Routes
Route::get('/app/fetch/totalfilms', function() {
    $count = DB::table('users')->count();
    return $count;
});

Route::post('/app/add/quick', ['uses' => 'AppController@Quick'])->name('quick-add');

Auth::routes();

Route::get('/app', ['uses' => 'AppController@Dashboard'])->name('app');
Route::get('/app/add', ['uses' => 'AppController@Add'])->name('add-movie');
