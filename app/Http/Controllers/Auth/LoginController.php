<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/app';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    protected function redirectTo()
    {
        return '/app';
    }

    public function logout() 
    {
        Auth::logout();
        return redirect(route('applogin'));
    }

    /**
     * Return the login form if the user is not logged in
     *
     * @return void
     */
    public function LoginForm()
    {
        if(Auth::guest()) :
            return view('home');
        else:
            return redirect(route('app'));
        endif;
    }
}
