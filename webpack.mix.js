const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */;
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

mix.js('resources/js/app.js', 'public/js')
    .extract(['vue'])
    .sass('resources/sass/app-critical.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css');

mix.webpackConfig({
    plugins: [
        new BrowserSyncPlugin({
            open: 'external',
            proxy: 'medialib.abc',
            files: ['resources/views/**/*.php', 'app/**/*.php', 'routes/**/*.php']
        })
    ]
});