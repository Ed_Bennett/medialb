# medialb

## To do list for app
- [ ] Select Front-end language/framework
- [ ] Select Back-end language/framework
- [ ] Create Login system
- [ ] Create Database system
- [ ] Create screens for add/remove/edit movies
- [ ] Create screens for showing movies
- [ ] Create screens for filtering results
- [ ] API Setup with TheMovieDB
- [ ] Filter imagery to be minified (As high quality as possible)
- [ ] Work out options to stream to TV
